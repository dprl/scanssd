# ScanSSD-XYc: Scanning Single Shot Detector for Graphics in Document Images

**Authors:** Abhisek Dey, Parag Mali, Matt Langsenkamp, and Richard Zanibbi  
[Dcoument and Pattern Recognition Lab (dprl)](https://www.cs.rit.edu/~dprl), Rochester Institute of Technology, USA

A [PyTorch](http://pytorch.org/) implementation of ScanSSD-XYc
[(GREC paper)](https://www.cs.rit.edu/~rlaz/files/ScanSSDv2.pdf) by **Abhisek Dey**.
It was developed using SSD implementation by [**Max
deGroot**](https://github.com/amdegroot) and is a lighter, more efficient
alternative to the original [ScanSSD](https://github.com/MaliParag/ScanSSD)
system by Parag Mali.

Developed using Cuda 11.2 and Pytorch 1.3.0

## Table of Contents
- <a href='#installation'>Installation</a>
- <a href='#code-organization'>Code Organization</a>
- <a href='#training-scanssd'>Training</a>
- <a href='#testing'>Testing</a>
- <a href='#evaluate'>Evaluate</a>
- <a href='#related-publications'>Related Publications</a>

## Installation (Conda)
We highly recommend using conda for using this system as it will be easier to download and install all the required dependencies. You can either choose to install the packages manually or using *Makefile*.
**Using the *Makefile* option will automatically install the conda environment, install the required dependencies, download the pre-trained weights and 
automate the training and testing routines.** 

First make sure you have Anaconda3 installed on your system. Then run the following commands (manually or automatically using _Makefile_).

### Using Makefile
From the root of this repo run `make`. It will setup all the required environments and paths
as needed and download the pre-trained weights.

### Manually
```zsh
$ conda create -n scanssd python=3.6.9
$ conda activate scanssd
(scanssd) $ pip install -r requirements.txt
```

To run using the GTDB dataset, Download the dataset by following the instructions on (https://github.com/MaliParag/TFD-ICDAR2019).
## Code Organization

SSD model is built in `ssd.py`. Training and testing the SSD is managed in `train.py` and `test.py`. All the training code is in `layers` directory. Hyper-parameters for training and testing can be specified through command line and through `config.py` file inside `data_loaders` directory. 

`data_loaders` directory also contains `gtdb_iterable.py` data reader that uses sliding windows to generates sub-images of page for training. All the scripts for pooling the sub-image level detections and XY Cuts are in `utils` directory. 

Functions for data augmentation, visualization of bounding boxes and heatmap are also in `utils`. 

## Setting up your own data for training

If you are not sure how to setup data, use [dir_struct directory](https://github.com/MaliParag/ScanSSD/blob/master/dir_struct) file. It shows an example directory structure that you can use for setting up data for training and testing. 

To generate .pmath files (csv files containing only numbers for bounding-box coordinates, 1 per page) or .pchar (same as .pmath but contains character-based box coordinates) files you can use [this](https://github.com/MaliParag/ScanSSD/blob/master/gtdb/split_annotations_per_page.py) script. 

## Training ScanSSD
### Using Makefile
If installed using Makefile, run `make train-example`. This should start the training automatically on the example PDF document. The weights per epoch would be saved in the `src/weights_ScanSSD_XY_train` folder.

### Manually
- First download the fc-reduced [VGG-16](https://arxiv.org/abs/1409.1556) PyTorch base network weights [here](https://drive.google.com/file/d/1GqiyZ1TglNW5GrNQfXQ72S8mChhJ4_sD/view?usp=sharing)
- By default, we assume you have downloaded the file in the `src/base_weights` dir.
- From the `<root>` of this repo, export PYTHONPATH: `export PYTHONPATH="${PYTHONPATH}:${PWD}"`
  You may add this path to your `.bashrc` profile to avoid exporting it everytime.

- Example Run command - For training with a subsample of the TFD-ICDARv2 dataset on GPU 0. 
- The trained weights will appear in `src/weights_<exp_name>`
```Shell
python3 src/train.py \
--dataset GTDB \
--dataset_root quick_start_data \
--cuda True \
--visdom False \
--batch_size 4 \
--num_workers 4 \
--exp_name ScanSSD_XY_train \
--model_type 512 \
--suffix _512 \
--training_data file_lists/quick_start_train \
--cfg math_gtdb_512 \
--loss_fun ce \
--kernel 1 5 \
--padding 0 2 \
--neg_mining True \
--stride 0.05 \
--gpu 0
```

### Visualizing Training Using TensorBoard
While training is in progress, you can visualize the training trends in Tensorboard. To use TensorBoard, in a separate terminal, first activate the `scanssd` conda environment. From the root of this repo run:

```tensorboard --logdir=src/TensorBoard_Logs/ScanSSD_XY_train```
Start your local web browser and go to `http://localhost:6006/` to visualize the training curves. 

- Note:
  * For training, an NVIDIA GPU is strongly recommended for speed.
  * You can pick-up training from a checkpoint by specifying the path as one of the training parameters (again, see `train.py` for options)

## Testing
## Pre-Trained weights

**Note:** Skip this step if ScanSSD-XYc installed using `make` (the _Makefile_).

For quick testing, pre-trained weights are available [here](https://drive.google.com/file/d/1CG8Z6R-BS9SL2ntFo8ruJhWbg8yaIuik/view?usp=sharing).
Download and place it in the `src/trained_weights` directory.


### Using Makefile
If installed using makefile, run `make test-example`. The outputs should be generated in
`src/eval/SSD`.

### Manually
To test a trained network (Make sure you have added this to PYTHONPATH):

```Shell
python3 src/test.py \
--save_folder src/eval/ \
--cuda True \
--dataset_root quick_start_data/ \
--model_type 512 \
--trained_model src/trained_weights/ssd512GTDB_256_epoch15.pth \
--cfg math_gtdb_512 \
--padding 0 2 \
--kernel 1 5 \
--batch_size 4 \
--log_dir src/logs/ \
--test_data file_lists/quick_start \
--stride 1.0 \
--post_process 0 \
--conf 0.5 \
--gpu 0
```

### Visualize results

**Note:** If you have used makefile to generate predictions, activate conda first by using `conda activate scanssd` to make sure all the package dependencies are met.

After prediction have been generated you can overlay them over the original image with the script below. You can also optionally 
overlay the grounds truths as well. Run `python src/utils/viz_final_boxes.py --help` for more information. The script will generate an output image
containing the predictions (in <span style="color:green">green</span>) boxes overlaid with the actual ground-truth boxes
(in <span style="color:red">red</span>) boxes. The image will be saved in the root directory (from where the program was called).

```shell
python src/utils/viz_final_boxes.py \
--predcsv src/eval/SSD/conf_0.5/Emden76.csv \
--gtcsv quick_start_data/gt/Emden76.csv \
--pagenum 1 \
--imgpath quick_start_data/images/Emden76/2.png
```

## Evaluate
You need to activate the conda environment if not already done so (`conda activate scanssd`).

You can evaluate the detections compared to the ground truth. The csv's should be named after
the pdf document names and should contain the boxes in the format:

`page_num, min_x, min_y, max_x, max_y`

From the root directory:
```Shell
python3 IOU_lib/IOUevaluater.py \
--ground_truth quick_start_data/gt/ \
--detections src/eval/SSD/conf_0.5/ \
--exp_name ScanSSD_XY
```

Where:

`--ground_truth` - GT CSV files for the documents tested

`--detections` - Detection folder for the epoch and confidence level

`--exp_name` - The detection result will be saved in a csv in `ssd/metrics/<exp_name>/<weight_name>.csv`

### Evaluate on multiple weights and multiple confidence levels (When testing is run on multiple confidence levels)
You can also use the same script to evaluate multiple epoch results (with multiple confidence threshold levels). Run the script on the epochs you want to evaluate keeping the directory as:

**Note:** Make sure that your detection folder structure follows the following structure:

`test_save_folder/ssd512GTDB_epoch<number>/conf_<level>/<doc_names>.csv`

The entire directory structure should look like:

```
<test_save_folder>
  ssd512GTDB_epoch1
    conf_0.1
      <CSVs>
    conf_0.25
      <CSVs>
    conf_0.5
      .
      .
      .
  ssd512GTDB_epoch2
    .
    .
    .   
```
For using IOUevaluater on a folder containing prediction csv files for different epochs at different confidence levels, use the following format for each epoch:
```Shell
python IOU_lib/IOUevaluater.py \
--ground_truth quick_start_data/gt \
--det_folder ssd/eval/<detection_folder> \
--exp_name <Name of Exp Folder>
```

Where:

`--det_folder` - The detection folder for an epoch. From the above structure it would be 
**ssd512GTDB_epoch1** or **ssd512GTDB_epoch2** and so on 

**Note:** Keep the `exp_name` same for all epochs for the same testing run.

You have to run each weight file you want to evaluate separately and keep the same `exp_name` for the same set of weights. It can handle one or many confidence levels.
For each epoch your experiment folder will have a corresponding output csv file named `ssd512GTDB_epoch<number>_overall.csv`.
The experiment folder will be in the `ScanSSD/ssd/metrics` directory

### Determining best weight file to use
After you have obtained weight files you can choose a metric out of 
```python
metrics = ['F_0.01', 'P_0.01', 'R_0.01', 'F_0.25', 'P_0.25', 'R_0.25', 'F_0.5', 'P_0.5', 'R_0.5', 'F_0.75', 'P_0.75', 'R_0.75', 'F_1.0', 'P_1.0', 'R_1.0']
```
Where `R_0.5` would give the best weight file and recall value at 50% IoU, `F_0.75` would give the best F-score at 75% IoU and so on.

You can use the following:
```shell
python IOU_lib/get_best.py \
--exp_folder <Path to the CSV outputs containing metrics> \
--metrics <One or many metrics to compare>
```

**Note:** Metrics can be one or many. For eg: `--metrics R_0.5 R_0.75` or `--metrics F_0.5`. The program will give you the best weight to use for each metric.

## Related publications

Dey, Abhisek et al. ["ScanSSD-XYc: Faster Detection of Math Formulas"](https://www.cs.rit.edu/~rlaz/files/ScanSSDv2.pdf), GREC 2021.

Mali, Parag, et al. “ScanSSD: Scanning Single Shot Detector for Mathematical Formulas in PDF Document Images.” ArXiv:2003.08005 [Cs], Mar. 2020. arXiv.org, http://arxiv.org/abs/2003.08005.

P.S. Mali, ["Scanning Single Shot Detector for Math in Document Images."](https://scholarworks.rit.edu/theses/10210/) Master's Thesis, Rochester Institute of Technology, 2019.

M. Mahdavi, R. Zanibbi, H. Mouchere, and Utpal Garain (2019). [ICDAR 2019 CROHME + TFD: Competition on Recognition of Handwritten Mathematical Expressions and Typeset Formula Detection.](https://www.cs.rit.edu/~rlaz/files/CROHME+TFD%E2%80%932019.pdf) Proc. International Conference on Document Analysis and Recognition, Sydney, Australia (to appear).

## Contributors and Acknowledgements

- Abhisek Dey: Created faster ScanSSD-XYc (this version)
- Parag Mali: Created the original ScanSSD system
- Matt Langsenkamp: Server and containerization
- Richard Zanibbi: Additional programming
- [**Max deGroot**](https://github.com/amdegroot) created the open-source SSD code used in the system; please see the [ssd.pytorch](https://github.com/amdegroot/ssd.pytorch) library for licensing information, etc.

ScanSSD/SSD-XYc code: Copyright (c) 2019-2021 Abhisek Dey, Parag Mali, Matt Langsenkamp, and Richard Zanibbi

## Support

This material is based upon work supported by the National Science Foundation
(USA) under Grant Nos. IIS-1717997 (MathSeer), and 2019897 (MMLI), and the
Alfred P. Sloan Foundation under Grant No. G-2017-9827.  

Any opinions, findings and conclusions or recommendations expressed in this
material are those of the author(s) and do not necessarily reflect the views of
the National Science Foundation or the Alfred P. Sloan Foundation.
